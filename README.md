# Tracemail

Tracemail is a lean tool to help user admin to trace emails sent and received on Postfix platform. It has a web front end and a background task to read /var/log/maillog and store transactions in Mariadb. Tracemail is licensed under GPL v3.

## Getting Started

See the INSTALL file.

### Prerequisites

Developed and tested on a Centos 7 system with Perl 5.16 and PHP 5.4.16.  

### Installing

See the INSTALL file.

## Contributing

Please contribute.

## Versioning

Please suggest.

## Authors

* **Man Kai KAM** - *Initial work* - https://gitlab.com/fkam18/tracemail


## License

This project is licensed under the GPL V3.

## Acknowledgments

* Hat tip to anyone who's code was used
* Great experience with Mailwatch
* Perl's super cool regex

