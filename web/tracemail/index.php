<!--
#    Copyright (C) 2018 Man Kai KAM
#    This file is part of Tracemail.
#
#    Tracemail is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Tracemail is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Tracemail.  If not, see <http://www.gnu.org/licenses/>.
-->
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tracemail</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2>Tracemail (show latest 200)</h2>
  <p>
 <form action="/tracemail/index.php" method="post">
  <div class="form-group">
    <label for="tag">Search (sender/receivers):</label>
    <input type="text" class="form-control" id="tag" name="tag">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form> 
</p>
  <table class="table">
    <thead>
      <tr>
        <th>Date</th>
        <th>From</th>
        <th>To</th>
        <th>Subject</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
<?php include 'table.php'; ?>
    </tbody>
  </table>
</div>

</body>
</html>
