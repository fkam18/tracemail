<!--
#    Copyright (C) 2018 Man Kai KAM
#    This file is part of Tracemail.
#
#    Tracemail is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Tracemail is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Tracemail.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
$tag = "";
if (isset($_POST["tag"])) {
  $tag = $_POST["tag"];
}
//echo '<pre>'; print_r($_POST); echo '</pre>';

// Open connection
try 
{
	$pdo = new PDO('mysql:host=localhost;dbname=tracemail', 'tracemail', '123oper');

}
catch (PDOException $e) 
{
    echo 'Error: ' . $e->getMessage();
    exit();
}
// Run Query
if ($tag == "") {
$sql 	= 'SELECT a.date, a.id1, a.sender, b.receiver, a.subject, b.status FROM log a, delivery b where a.id2 = b.id1 order by a.date DESC LIMIT 200';
}
else {
$sql 	= "SELECT a.date, a.id1, a.sender, b.receiver, a.subject, b.status FROM log a, delivery b where (a.id2 = b.id1) and (a.sender LIKE '%$tag%' or b.receiver LIKE '%$tag%') order by a.date DESC LIMIT 200";
}
$stmt 	= $pdo->prepare($sql); // Prevent MySQl injection. $stmt means statement
$previd = "";
$stmt->execute();
while ($row = $stmt->fetch())
{
   if ($previd <> $row['id1']) {
     print("<tr>\n");
     echo "<td>" . $row['date'] . "</td>\n";
     echo "<td>" . $row['sender'] . "</td>\n";
   }
   else {
     echo "<td>" . "-" . "</td>\n";
     echo "<td>" . "-" . "</td>\n";
   }
   echo "<td>" . $row['receiver'] . "</td>\n";
   echo "<td>" . $row['subject'] . "</td>\n";
   echo "<td>" . $row['status'] . "</td>\n";
   if ($previd <> $row['id1']) {
     print("</tr>\n");
   }
   $previd = $row['id1'];
}

// Close connection
$pdo = null;
//print("
//      <tr>
//        <td>July</td>
//        <td>Dooley</td>
//        <td>july@example.com</td>
//      </tr>
//");


?>
